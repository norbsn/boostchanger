#!/bin/bash

pkgver=3.8

if [[ -d /opt/boostchanger-v$pkgver ]]
then
    rm -rf /opt/boostchanger-v$pkgver
    rm /usr/bin/boostchanger
    rm /usr/share/applications/boostchanger.desktop
    rm /usr/share/pixmaps/boostchanger.png
    echo "Boost Changer is successfully uninstalled"
fi 
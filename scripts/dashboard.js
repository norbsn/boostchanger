// All of the Node.js APIs are available in the preload process.
// It has the same sandbox as a Chrome extension.
const sys_info = require('systeminformation')

// close app if user hit close button
const closeApp = document.getElementById('close');
closeApp.addEventListener('click', () => {
  window.close();
});
//os name
sys_info.osInfo().then((os_name) => {
  document.getElementById("os_name").innerHTML = os_name.distro
})

//Kernel name
sys_info.osInfo().then((kernel) => {
  document.getElementById("kernel").innerHTML = kernel.kernel
})

// Up time 
var timeInSec = sys_info.time().uptime // time will be in Sec
var timeInHour = timeInSec / 3600 // change time from Sec to Hours
document.getElementById("up_time").innerHTML = timeInHour.toFixed(2) + " hours"

// cpu name
sys_info.cpu().then((cpu_name) => {
  document.getElementById("cpu_name").innerHTML = cpu_name.manufacturer + " " + cpu_name.brand + " " + cpu_name.speed + "GHz"
})

// memory
sys_info.mem().then(
  (total_memory) => {
    // Total
    var total_memoryInByte = total_memory.total
    var total_memoryInGB = total_memoryInByte / Math.pow(1024, 3)
    document.getElementById("mem_total").innerHTML = total_memoryInGB.toFixed(0) + " GB"
  })

// available memory
sys_info.mem().then((free_memory) => {
  var free_memoryInByte = free_memory.available
  var free_memoryInGB = free_memoryInByte / Math.pow(1024, 3)
  document.getElementById("free_mem").innerHTML = free_memoryInGB.toFixed(2) + " GB"
})

// function for the interval to update free mem every 25 seconds
setInterval(() => {
  sys_info.mem().then((free_memory) => {
    var free_memoryInByte = free_memory.available
    var free_memoryInGB = free_memoryInByte / Math.pow(1024, 3)
    document.getElementById("free_mem").innerHTML = free_memoryInGB.toFixed(2) + " GB"
  })
}, 25000) // 25000 millisecond = 25 seconds

// Local IP
sys_info.networkInterfaces().then((data) => {
  document.getElementById("local_ip").innerHTML = data[1].ip4
})

// Public IP 
const publicIp = require('public-ip');
publicIp.v4().then((ip_public) => {
  document.getElementById("public_ip").innerHTML = ip_public
})


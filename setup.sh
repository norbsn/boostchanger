#!/bin/bash

pkgver=3.8
pkgverOLD=3.6

if [[ -d /opt/boostchanger-v$pkgverOLD ]]
then
    rm -rf /opt/boostchanger-v$pkgverOLD
    rm /usr/bin/boostchanger
    rm /usr/share/applications/boostchanger.desktop
    rm /usr/share/pixmaps/boostchanger.png
    cd app/
    cp -r boostchanger-v$pkgver /opt
    install -Dm755 boostchanger.sh /usr/bin/boostchanger
    install -Dm644 boostchanger.desktop /usr/share/applications/boostchanger.desktop
    install -Dm644 boostchanger.png /usr/share/pixmaps/boostchanger.png
    echo "Boost Changer is successfully installed"
else
    cd app/
    cp -r boostchanger-v$pkgver /opt
    install -Dm755 boostchanger.sh /usr/bin/boostchanger
    install -Dm644 boostchanger.desktop /usr/share/applications/boostchanger.desktop
    install -Dm644 boostchanger.png /usr/share/pixmaps/boostchanger.png
    echo "Boost Changer is successfully installed"
fi 


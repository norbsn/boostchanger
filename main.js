// Modules to control application life and create native browser window
const { app, BrowserWindow, Menu, Tray } = require('electron')
const path = require('path')
const windowStateKeeper = require('electron-window-state')

// define all important variables
let mainWindow, errorWindow, helpWindow, tray, mainMenu, trayMenu, winState

// App Menu
mainMenu = Menu.buildFromTemplate(require('./mainMenu'));

// define tray
function trayApp() {
  tray = new Tray(path.join(__dirname, 'icon/boostChanger.png'));
  tray.setToolTip('Boost Changer');
  trayMenu = Menu.buildFromTemplate([
    {
      label: 'Show App', click: () => {
        mainWindow.show();
      }
    },
    {
      label: 'Exit', role: 'quit'
    },
  ])
  tray.setContextMenu(trayMenu);
} //End Tray

// Define Error Window
function openErrorWindow() {
  errorWindow = new BrowserWindow({
    height: 160,
    resizable: false,
    width: 320,
    title: 'Error',
    minimizable: false,
    webPreferences: {
      nodeIntegration: true,
    },
    show: false, //When all the content of the app has been loaded, then the app will show up
    icon: path.join(__dirname, 'icon/boostChanger.png')
  })
  // About window
  errorWindow.loadFile(path.join(__dirname, 'frontend/error.html'))
  //When all the content of the app has been loaded, then the app will show up
  errorWindow.once('ready-to-show', errorWindow.show)
  //don't show any Menu-bar in this window
  errorWindow.setMenu(null)

  // errorWindow.webContents.openDevTools()

  //Garbage collection handle
  errorWindow.on('closed', () => {
    errorWindow = null
  })
}

function createWindow() {
  // call trayApp function
  trayApp()

  // // Window state manager
  // winState = windowStateKeeper({
  //   defaultWidth: 750,
  //   defaultHeight: 450,
  // })

  // Create the main window.
  mainWindow = new BrowserWindow({
    width: 850,
    height: 550,
    x: windowStateKeeper().x,
    y: windowStateKeeper().y,
    // width: winState.width,
    // height: winState.height,
    // x: winState.x,
    // y: winState.y,
    resizable: false, //TODO for DEV
    title: 'Boost Changer v3.8',
    webPreferences: {
      //preload: path.join(__dirname, 'preload.js'),
      nodeIntegration: true,
    },
    show: false, //When all the content of the app has been loaded, then the app will show up
    icon: path.join(__dirname, 'icon/boostChanger.png')
  })

  // When user minimize, the app will disappear 
  mainWindow.on('minimize', (event) => {
    event.preventDefault();
    mainWindow.hide();
  })

  // load the index.html of the app.
  mainWindow.loadFile('frontend/index.html')

  // When all the content of the app has been loaded, then the app will show up
  mainWindow.once('ready-to-show', mainWindow.show)

  // set Main for the main Window
  Menu.setApplicationMenu(mainMenu);

  // TODO: for DEV Open the DevTools. 
  // mainWindow.webContents.openDevTools()

  // manage winState for the main Window
  windowStateKeeper().manage(mainWindow)

  //the entire App will close
  mainWindow.on('close', () => { app.quit(); });

  //Garbage collection handle
  mainWindow.on('closed', () => {
    mainWindow = null
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  // const rel = globalShortcut.register("Control+X", () => {
  //   console.log("Control+X has been pressd");
  // })
  //TODO This code for DEV
  createWindow();
  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })//TODO:TILL HERE

  // const fs = require('fs')
  // //show error when the user uses VM 

  // if (fs.existsSync('/sys/devices/system/cpu/intel_pstate/no_turbo')) {
  //   createWindow()

  //   app.on('activate', () => {
  //     if (BrowserWindow.getAllWindows().length === 0) createWindow()
  //   })
  // } else {
  //   openErrorWindow()
  //   app.on('activate', () => {
  //     if (BrowserWindow.getAllWindows().length === 0) openErrorWindow()
  //   })
  // }
})

// Definition for new window (Help)
function openHelpWindow() {
  helpWindow = new BrowserWindow({
    height: 300,
    resizable: false,
    width: 450,
    title: 'Help',
    minimizable: false,
    show: false,
    icon: path.join(__dirname, 'icon/boostChanger.png')
  })
  // Help window
  helpWindow.loadFile(path.join(__dirname, 'frontend/help.html'))
  helpWindow.once('ready-to-show', helpWindow.show)
  helpWindow.setMenu(null)
  //open external link from a normal browser only in about.html
  helpWindow.webContents.on("new-window", (e, url) => {
    e.preventDefault();
    require("electron").shell.openExternal(url);
  })

  //Garbage collection handle
  helpWindow.on('closed', () => {
    helpWindow = null
  })
}
module.exports = { openHelpWindow } // make this function as public 

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  app.quit();
});
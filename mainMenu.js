module.exports = [
    {
        label: "File",
        submenu: [{
            label: 'close',
            role: 'quit',
            accelerator: "Control+Q"
        }],
    },
    { label: '|' },
    {
        label: 'Help', click: () => {
            const helpWindow = require("./main.js")
            helpWindow.openHelpWindow()
        }
    },
];